# GIT Summary

## Basics of GIT
GIT is a version-control software that makes it easy for the individuals to keep track and view the changes made on the files by the users.  
It makes collaboration with team mates a very simple task.  

![image](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTkGCM6c1OXM87fgPGJIYXKHpfY30NoHUzKGA&usqp=CAU)

## Terminologies used
1. Repository: A repository is like a folder for the project. It contains all the project files and stores each file's revision history.One can also discuss and manage a project's work within the repository.    
2. GitLab: It is a web-based Git repository that provides free open and private repositories, issue-fllowing capabilities and wikis.  
3. Commit:This command is used to save the changes made on the local repository.  
4. Push:This command uploads the content in the local repo to remote repo.  
5. Branch:It is a way to keep developing and coding a new feature or modification to the software and still not affecting the main part of the project.  
6. Merge: Combining or integrating 2 or more branches into a single branch.  
7. Clone: It is a method by which an exact copy of the entire online repo is copied into the local machine.
8. Fork: A new repo is created of the existing online repo.  

## Basic Commands
The command to install git on a computer using Linux OS is:
**sudo apt-get install get**

## Git Internals and Workflow

Git has 3 main stages that the file can reside in:  
1. Modified: The file has been changed but not committed to the repo.  
2. Staged: Marked a modified file in its current version to go to next snapshot.  
3. Committed: Data is safely stored in the local repo in the form of pictures/snapshots. 

![workflow](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQDLktLwy4wWAyNYrfFZjUhgYOBDZjtWHzf_w&usqp=CAU)



Some of the commands for the workflow are:
1. To clone the repo  
**$ git clone _<link-to-repository>_**  
2. To create a new branch  
**$ got checkout master**  
**$ git checkout-b _<your-branch-name>_**  
3. To add untracked files  
**$ git add .**  
4. Description abot the commit  
**$ git commit -sv**  
5. To push changes into repo  
**$ git push origin _<branch-name>_**  

![git](https://www.nobledesktop.com/image/blog/git-branches-merge.png)
